/*******************************************************************************
* Copyright (C) Maxim Integrated Products, Inc., All Rights Reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
* OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL MAXIM INTEGRATED BE LIABLE FOR ANY CLAIM, DAMAGES
* OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of Maxim Integrated
* Products, Inc. shall not be used except as stated in the Maxim Integrated
* Products, Inc. Branding Policy.
*
* The mere transfer of this software does not imply any licenses
* of trade secrets, proprietary technology, copyrights, patents,
* trademarks, maskwork rights, or any other form of intellectual
* property whatsoever. Maxim Integrated Products, Inc. retains all
* ownership rights.
*******************************************************************************/

/// @file
/// @brief Defines short macro names without the %MaximInterfaceTest prefix.

#ifndef MaximInterfaceTest_TestShortMacros_hpp
#define MaximInterfaceTest_TestShortMacros_hpp

#include "Test.hpp"

#define MESSAGE MaximInterfaceTest_MESSAGE

#define INFO MaximInterfaceTest_INFO
#define WARNING MaximInterfaceTest_WARNING
#define ERROR MaximInterfaceTest_ERROR
#define FATAL MaximInterfaceTest_FATAL

#define ASSERT_TRUE MaximInterfaceTest_ASSERT_TRUE
#define ASSERT_TRUE_ELSE MaximInterfaceTest_ASSERT_TRUE_ELSE

#define ASSERT_FALSE MaximInterfaceTest_ASSERT_FALSE
#define ASSERT_FALSE_ELSE MaximInterfaceTest_ASSERT_FALSE_ELSE

#define ASSERT_EQUAL MaximInterfaceTest_ASSERT_EQUAL
#define ASSERT_EQUAL_ELSE MaximInterfaceTest_ASSERT_EQUAL_ELSE

#define ASSERT_NOT_EQUAL MaximInterfaceTest_ASSERT_NOT_EQUAL
#define ASSERT_NOT_EQUAL_ELSE MaximInterfaceTest_ASSERT_NOT_EQUAL_ELSE

#define ASSERT_LESS MaximInterfaceTest_ASSERT_LESS
#define ASSERT_LESS_ELSE MaximInterfaceTest_ASSERT_LESS_ELSE

#define ASSERT_LESS_EQUAL MaximInterfaceTest_ASSERT_LESS_EQUAL
#define ASSERT_LESS_EQUAL_ELSE MaximInterfaceTest_ASSERT_LESS_EQUAL_ELSE

#define ASSERT_GREATER MaximInterfaceTest_ASSERT_GREATER
#define ASSERT_GREATER_ELSE MaximInterfaceTest_ASSERT_GREATER_ELSE

#define ASSERT_GREATER_EQUAL MaximInterfaceTest_ASSERT_GREATER_EQUAL
#define ASSERT_GREATER_EQUAL_ELSE MaximInterfaceTest_ASSERT_GREATER_EQUAL_ELSE

#endif
